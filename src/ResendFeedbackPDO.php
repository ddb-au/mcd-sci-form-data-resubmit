<?php
namespace ResendFeedback;
use PDO;

class ResendFeedbackPDO extends PDO {
	private $configFile = 'conf/db.conf';
	function __construct() {
		if (!$settings = parse_ini_file($this->configFile, TRUE)) throw new exception('Unable to open ' . $this->configFile . '.');
		$dns = $settings['driver'] .
		       ':host=' . $settings['host'] .
		       ((!empty($settings['port'])) ? (';port=' . $settings['port']) : '') .
		       ';dbname=' . $settings['schema'];
		parent::__construct($dns, $settings['user'], $settings['password']);
	}
}