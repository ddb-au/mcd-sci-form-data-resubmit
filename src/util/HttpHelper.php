<?php
namespace ResendFeedback\Util;
use Exception;
class HttpHelper {
	protected $url = 'https://portal.fast-insight.com/custom/mcdonalds/mcdonalds_importSurvey.aspx';
	/**
	 * Send data to ISC
	 * @param $request_string
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function post_data_to_isc($request_string)
	{
		$curl = curl_init();
		$curl_opt_array = array(
			CURLOPT_URL => $this->url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $request_string,
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: multipart/form-data; boundary=---011000010111000001101001",
				"postman-token: 39235c15-5ebe-c2f9-f70e-31a743ba0395",
				"sharedsecret: 2150a878a4d64d3cb7167f5b20aa7d7769ba09107d084667a0ef2c0c3004a5ca"
			)
		);
		curl_setopt_array($curl, $curl_opt_array);

		$response = curl_exec($curl);
		$err = curl_error($curl);
		if ($err) {
			throw new Exception($err);
		}

		$decoded_response = json_decode($response, true);

		if (empty($decoded_response['response']) || trim(strtolower($decoded_response['response'])) != 'ok')
		{
			throw new Exception($response);
		}

		curl_close($curl);

		return $response;
	}
}
