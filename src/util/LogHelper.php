<?php
namespace ResendFeedback\Util;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Log\LogLevel;

class LogHelper {
	/**
	 * @var \Monolog\Logger The logger object
	 */
	protected $logger;
	/**
	 * @var array the fixed context to be added
	 */
	protected $context;

	public function __construct($logStreamPath , $config=[]) {
		$logger = new Logger("resend");
		$logger->pushHandler(new RotatingFileHandler($logStreamPath,LogLevel::DEBUG));
		$this->logger = $logger;
		$this->context = @$config["context"] ? : [];
	}

	/**
	 * Get context array by value
	 * @return array
	 */
	public function getContext(){
		return $this->context;
	}

	/**
	 * set context array
	 * @param $context
	 */
	public function setContext($context) {
		$this->context = $context;
	}

	/**
	 * @return array
	 */
	public function &getContextRef(){
		return $this->context;
	}

	/**
	 * @param $message
	 * @param array $context
	 */
	public function alert($message,$context=[]){
		$context = array_replace($this->context,$context);
		$this->logger->addAlert($message,$context);
	}

	/**
	 * @param $message
	 * @param array $context
	 */
	public function info($message,$context = []){
		$context = array_replace($this->context,$context);
		$this->logger->addInfo($message,$context);
	}
}