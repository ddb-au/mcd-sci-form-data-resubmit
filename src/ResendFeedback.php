<?php
namespace ResendFeedback;
use ResendFeedback\DatabaseConnectionFactory;
use ResendFeedback\Models\FeedbackFieldModel;
use ResendFeedback\Models\WebSubmissionModel;
use ResendFeedback\Models\WebFormDataModel;
use ResendFeedback\Util\HttpHelper;
use ResendFeedback\Util\LogHelper;
use Exception;

class ResendFeedback {
	private $feedbackFormFields;
	private $submissions;
	private $webFormDataModel;
	private $cidQuestionPositionMapping;
	private $httpHelper;
	private $failLogger;
	private $successLogger;
	function __construct() {
		$connection = DatabaseConnectionFactory::connect();
		$feedbackFieldModel = new FeedbackFieldModel($connection,DatabaseConnectionFactory::$views['feedback_form_fields_view']);
		$this->feedbackFormFields = $feedbackFieldModel->getAllFields();
		$webSubmissionModel = new WebSubmissionModel($connection,DatabaseConnectionFactory::$views['temp_form_submissions_view']);
		$this->submissions = $webSubmissionModel->getAllSubmission();
		$this->webFormDataModel = new WebFormDataModel($connection,DatabaseConnectionFactory::$views['temp_form_submit_data_view']);
		$this->httpHelper = new HttpHelper();
		$this->failLogger = new LogHelper(__DIR__.'/../logs/resend_failed.log');
		$this->successLogger = new LogHelper(__DIR__.'/../logs/resend_success.log');
	}
	function formDataBuilder(){
		$dataQueue = array();
		foreach($this->submissions as $index => $submission){
//			if($index < $caseNumber){
				$dataItems = $this->webFormDataModel->getDataBySid($submission->sid);
				if($dataItems !== null) {
					$dataQueueItems = array();
					foreach($dataItems as $data) {
						$name = $this->get_field_via_cid($data->cid);
						$dataQueueItems[$name] = $data->data;
						$dataQueueItems['submitted'] = $data->submitted;
						$dataQueueItems['sid'] = $data->sid;
					}
					$dataQueue[$index] = $dataQueueItems;
				} else {
					throw new \Exception('no form data for ' . $submission->sid);
				}
//			} else {
//				break;
//			}
		}
		return $dataQueue;
	}

	function mcd_send_feedback_data($form_data, $sid, $submitted) {
		$is_valid = TRUE;
		$result = array(
			'send_success' => $is_valid,
			'view_data' => array (
				'sid' => $sid
			)
		);
		$australian_tz = [
			"vic" => "Australia/Melbourne",
			"wa"  => "Australia/Perth",
			"qld" => "Australia/Brisbane",
			"nsw" => "Australia/Sydney",
			"sa"  => "Australia/Adelaide",
			"nt"  => "Australia/Darwin",
			"act" => "Australia/Canberra",
			"tas" => "Australia/Hobart",
		];
		$when = $form_data[ "when_did_you_visit_us" ];

		$when_did_you_visit_us = NULL;

		if ($when == "today") {
			$when_did_you_visit_us = date('Y-m-d', $submitted);
		}
		else {
			if ($when == "yesterday") {
				$when_did_you_visit_us = date('Y-m-d', $submitted - 60 * 60 * 24);
			}
			else {
				if ($when == "other") {
					$date                  = explode('-', $form_data[ "date" ]);
					$when_did_you_visit_us = ($date[ 2 ] . "-" . $date[ 1 ] . "-" . $date[ 0 ]);
				}
			}
		}

		preg_match('#\[(.*?)\]#', $form_data[ "which_maccas_did_you_visit" ], $store);
		$storename = explode('[', $form_data[ "which_maccas_did_you_visit" ])[ 0 ];

		$store_state_tz = NULL;

		$which_maccas = $store[ 1 ];

		if (!empty($store_state_tz)) {
			$date      = new DateTime(NULL, new DateTimeZone($store_state_tz));
			$head_date = $date->format('Y-m-d');
			$head_time = $date->format('H:i:s');
		}
		else {
			// Timezone can't be found, email tech and defaulted to default tz.
			$head_date = date('Y-m-d', $submitted);
			$head_time = date('H:i:s', $submitted);
		}

		$isc_req_string = "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"meta\"\r\n\r\n{  \"locationStoreID\" : \"" . $which_maccas . "\",  \"headDate\" : \"" . $head_date . "\",  \"headTime\" : \"".$head_time."\",  \"campaign\" : \"N/A\" }\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"data\"\r\n\r\n[{   \"questionPosition\" : 1,   \"comment\" : null,   \"answer\" : [2]  }, {   \"questionPosition\" : 2,   \"comment\" : null,   \"answer\" : [" . intval($form_data["where_did_you_order_from"]) ."]  }, {   \"questionPosition\" : 3,   \"comment\" : null,   \"answer\" : [". intval($form_data["what_time_did_you_visit_us"]) ."]  }, {   \"questionPosition\" : 4,   \"comment\" : \"". $when_did_you_visit_us ."\",   \"answer\" : null  }, {   \"questionPosition\" : 5,   \"comment\" : null,   \"answer\" : [". intval($form_data["overall_satisfaction"]) ."]  }, {   \"questionPosition\" : 6,   \"comment\" : " . (empty($form_data["overall_satisfaction_other"]) ? "null" : "\"".str_replace('"', "'", $form_data["overall_satisfaction_other"])."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 7,   \"comment\" : null,   \"answer\" : [". intval($form_data["how_was_your_food"]) ."]  }, {   \"questionPosition\" : 8,   \"comment\" : null,   \"answer\" : " . ($form_data["oh_no_were_really_sorry_to_hear_that"] === NULL ? "null" : "[" . $this->ICS_FormatCheckboxVals($form_data["oh_no_were_really_sorry_to_hear_that"]) . "]") . "  }, {   \"questionPosition\" : 9,   \"comment\" : " . (empty($form_data["quality_other"]) ? "null" : "\"".str_replace('"', "'", $form_data["quality_other"]) ."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 10,   \"comment\" : null,   \"answer\" : " . ($form_data["how_friendly_were_we"] === NULL ? "null" : "[" . intval($form_data["how_friendly_were_we"]) . "]") . "  }, {   \"questionPosition\" : 11,   \"comment\" : null,   \"answer\" : " . ($form_data["friendly_bad"] === NULL ? "null" : "[" . $this->ICS_FormatCheckboxVals($form_data["friendly_bad"]) . "]") . "  }, {   \"questionPosition\" : 12,   \"comment\" : " . (empty($form_data["friendly_other"]) ? "null" : "\"".str_replace('"', "'", $form_data["friendly_other"])."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 13,   \"comment\" : null,   \"answer\" : [" . intval($form_data["accurate"]) ."]   }, {   \"questionPosition\" : 14,   \"comment\" : null,   \"answer\" : " . ($form_data["accurate_bad"] === NULL ? "null" : "[" . $this->ICS_FormatCheckboxVals($form_data["accurate_bad"]) . "]") . "  }, {   \"questionPosition\" : 15,   \"comment\" : " . (empty($form_data["accurate_other"]) ? "null" : "\"".str_replace('"', "'", $form_data["accurate_other"])."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 16,   \"comment\" : null,   \"answer\" : [" . intval($form_data["clean"]) ."]  }, {   \"questionPosition\" : 17,   \"comment\" : null,   \"answer\" : " . ($form_data["clean_bad"] === NULL ? "null" : "[" . $this->ICS_FormatCheckboxVals($form_data["clean_bad"]) . "]") . "  }, {   \"questionPosition\" : 18,   \"comment\" : " . (empty($form_data["clean_other"]) ? "null" : "\"".str_replace('"', "'", $form_data["clean_other"])."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 19,   \"comment\" : null,   \"answer\" : [" . intval($form_data["fast"]) ."] }, {   \"questionPosition\" : 20,   \"comment\" : null,   \"answer\" : " . ($form_data["fast_bad"] === NULL ? "null" : "[" . $this->ICS_FormatCheckboxVals($form_data["fast_bad"]) . "]") . "  }, {   \"questionPosition\" : 21,   \"comment\" : " . (empty($form_data["fast_other"]) ? "null" : "\"".str_replace('"', "'", $form_data["fast_other"])."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 22,   \"comment\" : null,   \"answer\" : [" . (intval($form_data["nps"])+1) ."]   }, {   \"questionPosition\" : 23,   \"comment\" : " . (empty($form_data["nps_9to10"]) ? "null" : "\"".str_replace('"', "'", $form_data["nps_9to10"])."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 24,   \"comment\" : " . (empty($form_data["nps_7to8"]) ? "null" : "\"".str_replace('"', "'", $form_data["nps_7to8"])."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 25,   \"comment\" : " . (empty($form_data["nps_0to6"]) ? "null" : "\"".str_replace('"', "'", $form_data["nps_0to6"])."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 26,   \"comment\" : null,   \"answer\" : " . (empty($form_data["mcdonalds_contact"]) ? "[2]" : "[". intval($form_data["mcdonalds_contact"]) ."]") . "}, {   \"questionPosition\" : 27,   \"comment\" : " . (empty($form_data["first_name"]) ? "null" : "\"".str_replace('"', "'", ($form_data["first_name"] . " " . $form_data["last_name"]))."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 28,   \"comment\" : " . (empty($form_data["details_email_address"]) ? "null" : "\"".str_replace('"', "'", $form_data["details_email_address"])."\"") . ",   \"answer\" : null  }, {   \"questionPosition\" : 29,   \"comment\" : " . (empty($form_data["details_phone_number"]) ? "null" : "\"".str_replace('"', "'", $form_data["details_phone_number"])."\"") . ",   \"answer\" : null  } ]\r\n-----011000010111000001101001--";

		try {
			$postRepsonse = $this->httpHelper->post_data_to_isc($isc_req_string);
			/* mark that record to draft */
			$connection = DatabaseConnectionFactory::connect();
			$webSubmissionModel = new WebSubmissionModel($connection,DatabaseConnectionFactory::$views['temp_form_submissions_view']);
			$webSubmissionModel->markDraftAfterSent($sid,1);
			$this->successLogger->info('[ISC_RESEND_SUCCESS]', ['sid'=>$sid,'data' => $isc_req_string,'response'=>$postRepsonse]);
			$result['view_data']['submitted'] = date('Y-m-d H:m:s');
		} catch (Exception $e) {
			/* change to logger instead of database record insertion */
			$this->failLogger->alert('[ISC_RESEND_FAIL]', ['sid'=>$sid, 'reason' => $e->getMessage(),'data' => $isc_req_string]);
			$result['send_success'] = false;
			$result['view_data']['detail'] = str_replace('"','',$e->getMessage());
			$result['view_data']['data'] = str_replace('"','',$isc_req_string);
			$result['view_data']['submitted'] = date('Y-m-d H:m:s');
		}

		return $result;
	}
	private function get_field_via_cid($cid){
		return $this->feedbackFormFields[$cid];
	}
	function ICS_FormatCheckboxVals($formVals)
	{
		$vals = "";
		if (is_array($formVals))
		{
			foreach($formVals as $value)
			{
				$vals .= $value . ", ";
			}
			$vals = rtrim($vals, ', ');
		}
		else
		{
			$vals .= $formVals;
		}
		return $vals;
	}
}