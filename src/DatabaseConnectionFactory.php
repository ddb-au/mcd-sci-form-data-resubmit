<?php
namespace ResendFeedback;

class DatabaseConnectionFactory {
	private static $viewsFile = 'conf/views.conf';
	public static $connection;
	public static $views;
	public static function disconnect(){
		self::$connection = null;
		return self::$connection;
	}
	public static function connect(){
		self::$connection = new ResendFeedbackPDO();
		if (!self::$views = parse_ini_file(self::$viewsFile, TRUE)) throw new exception('Unable to open ' . self::$viewsFile . '.');
		return self::$connection;
	}
}
