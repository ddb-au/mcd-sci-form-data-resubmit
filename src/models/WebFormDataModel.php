<?php
namespace ResendFeedback\Models;

class WebFormDataModel {
	public $connect;
	private $table;
	function __construct($connection , $table) {
		$this->connect = $connection;
		$this->table = $table;
	}
	function createObject($row) {
		$object = new \stdClass();
		$object->sid = $row->sid;
		$object->nid = $row->nid;
		$object->cid = $row->cid;
		$object->data = $row->data;
		$object->submitted = $row->submitted;
		return $object;
	}
	function getDataBySid($sid){
		$data = array();
		$query = $this->connect->prepare('select * from ' . $this->table . ' where sid = :sid');
		$query->bindParam(':sid',$sid);
		if($query->execute()){
			foreach($query->fetchAll() as $result){
				$item = $this->createObject((object)$result);
				array_push($data,$item);
			}
		}
		return $data;
	}
}