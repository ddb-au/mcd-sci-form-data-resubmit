<?php
namespace ResendFeedback\Models;

class FeedbackFieldModel {
	private $connect;
	private $table;
	function __construct($connection , $table) {
		$this->connect = $connection;
		$this->table = $table;
	}
	function createObject($row) {
		$object = new \stdClass();
		$object->cid = $row->cid;
		$object->name = $row->name;
		return $object;
	}
	function getAllFields(){
		$fields = array();
		$query = $this->connect->prepare('select * from feedback_form_fields');
		$query->execute();
		foreach($query->fetchall() as $result) {
			$feedbackField = (object)$result;
			$fields[$feedbackField->cid] = $feedbackField->form_key;
		}
		return $fields;
	}
	function getFieldNameViaCid($cid){
		$fieldname = '';
		$query = $this->connect->prepare('select name from feedback_form_fields where :cid');
		$query->bindParam(":cid",$cid);
		if($query->execute()){
			$fieldname = $query->fetch();
		}
		return $fieldname;
	}
}