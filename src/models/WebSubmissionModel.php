<?php
namespace ResendFeedback\Models;

class WebSubmissionModel {
	public $connect;
	private $table;
	function __construct($connection , $table) {
		$this->connect = $connection;
		$this->table = $table;
	}
	function createObject($row) {
		$object = new \stdClass();
		$object->sid = $row->sid;
		$object->nid = $row->nid;
		$object->uid = $row->uid;
		$object->is_draft = $row->is_draft;
		$object->submitted = $row->submitted;
		$object->remote_addr = $row->remote_addr;
		return $object;
	}
	function getAllSubmission(){
		$submissions = array();
		$query = $this->connect->prepare('select * from ' . $this->table . ' where is_draft=0');
		$query->execute();
		foreach($query->fetchall() as $index=>$result) {
			if($result['is_draft']){
				continue;
			}
			$submission = $this->createObject((object)$result);
			$submissions[] = $submission;
		}
		return $submissions;
	}
	function markDraftAfterSent($sid,$success=1){
		$query = $this->connect->prepare('update ' . $this->table . ' set is_draft='.$success . ' where sid='.$sid);
		return $query->execute();
	}
}