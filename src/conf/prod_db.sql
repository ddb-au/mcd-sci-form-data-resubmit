create or replace view temp_form_submissions
as select * from webform_submissions
where nid=1962 and sid>=1862688;

create or replace view temp_form_submit_data
as select webform_submitted_data.sid, webform_submitted_data.nid, webform_submitted_data.cid,webform_submitted_data.data,temp_form_submissions.submitted
from webform_submitted_data,temp_form_submissions
where temp_form_submissions.sid=webform_submitted_data.sid;

create view feedback_form_fields
as select * from `webform_component`
where nid = 1962;


select count(*) from temp_form_submissions;
select count(distinct(sid)) from temp_form_submit_data;