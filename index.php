<?php
require (__DIR__.'/vendor/autoload.php');
date_default_timezone_set("Australia/Sydney");
use ResendFeedback\ResendFeedback as ResendFeedback; ?>
<?php
if(@$_GET['send'] != null && @$_GET['sleep'] != null) :
    $sendNumber = intval($_GET['send']);
    $sleepSeconds = intval($_GET['sleep']);
else:
	$sendNumber = 100;
	$sleepSeconds = 5;
	$app = new ResendFeedback();
	$dataQueue = $app->formDataBuilder();
	$failed = array();
	$success = array();
	foreach($dataQueue as $index => $form_data) {
        $result = $app->mcd_send_feedback_data($form_data,$form_data['sid'],$form_data['submitted']);
        if($result['send_success']) {
//            array_push($success,$result['view_data']);
            echo $form_data['sid'].' -> success'.PHP_EOL;
        } else {
//            array_push($failed,$result['view_data']);
            echo $form_data['sid'].' ->  failed'.PHP_EOL;
        }
		if($index%$sendNumber == 0 && $index) {
			sleep($sleepSeconds);
		}
	}
endif;
exit;
?>
<!--<div style="width:100%; clear:both">-->
<!--	<div style="width:50%; float:left">-->
<!--		<h2 style="text-align: center">Success record table[--><?//=sizeof($success)?><!--]</h2>-->
<!--		<table>-->
<!--			<tbody>-->
<!--			<tr>-->
<!--				<th>sid</th>-->
<!--				<th>time</th>-->
<!--			</tr>-->
<!--			--><?php //foreach($success as $success_record): ?>
<!--				<tr>-->
<!--					<td>--><?//= $success_record['sid']?><!--</td>-->
<!--					<td>--><?//= $success_record['submitted']?><!--</td>-->
<!--				</tr>-->
<!--			--><?php //endforeach;?>
<!--			</tbody>-->
<!--		</table>-->
<!--	</div>-->
<!--	<div style="width:50%; float:left">-->
<!--		<h2 style="text-align: center">Failure record table [--><?//=sizeof($failed)?><!--]</h2>-->
<!--		<table>-->
<!--			<tbody>-->
<!--			<tr>-->
<!--				<th>sid</th>-->
<!--				<th>reason</th>-->
<!--				<th>time</th>-->
<!--				<th>details</th>-->
<!--			</tr>-->
<!--			--><?php //foreach($failed as $failed_record): ?>
<!--				<tr>-->
<!--					<td style="color:red">process---><?//=$failed_record['sid']?><!--</td>-->
<!--					<td style="color:red">--><?//= $failed_record['detail']?><!--</td>-->
<!--					<td>--><?//= $failed_record['submitted']?><!--</td>-->
<!--					<td>-->
<!--						<form method="post" action="/details">-->
<!--							<input type="hidden" name="sid" id="sid" value="--><?//= $failed_record['sid']?><!--">-->
<!--							<input type="hidden" name="detail" id="detail" value="--><?//= $failed_record['detail']?><!--">-->
<!--							<input type="hidden" name="data" id="data" value="--><?//= $failed_record['data']?><!--">-->
<!--							<input type="submit" value="View Detail">-->
<!--						</form>-->
<!--					</td>-->
<!--				</tr>-->
<!---->
<!--			--><?php //endforeach;?>
<!--			</tbody>-->
<!--		</table>-->
<!--	</div>-->
<!--</div>-->
